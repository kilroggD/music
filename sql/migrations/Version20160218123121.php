<?php

namespace DoctrineORMModule\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160218123121 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE acl_users_to_roles (users INT NOT NULL, roles INT NOT NULL, PRIMARY KEY(users, roles))');
        $this->addSql('CREATE INDEX IDX_1C77897C1483A5E9 ON acl_users_to_roles (users)');
        $this->addSql('CREATE INDEX IDX_1C77897CB63E2EC7 ON acl_users_to_roles (roles)');
        $this->addSql('ALTER TABLE acl_users_to_roles ADD CONSTRAINT FK_1C77897C1483A5E9 FOREIGN KEY (users) REFERENCES "users" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE acl_users_to_roles ADD CONSTRAINT FK_1C77897CB63E2EC7 FOREIGN KEY (roles) REFERENCES acl_roles (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE acl_users_to_roles');
    }
}
