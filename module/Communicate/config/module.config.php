<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Communicate\\V1\\Rest\\Methods\\MethodsResource' => 'Communicate\\V1\\Rest\\Methods\\MethodsResourceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'communicate.rest.methods' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/methods[/:methods_id]',
                    'defaults' => array(
                        'controller' => 'Communicate\\V1\\Rest\\Methods\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'communicate.rest.methods',
        ),
    ),
    'zf-rest' => array(
        'Communicate\\V1\\Rest\\Methods\\Controller' => array(
            'listener' => 'Communicate\\V1\\Rest\\Methods\\MethodsResource',
            'route_name' => 'communicate.rest.methods',
            'route_identifier_name' => 'methods_id',
            'collection_name' => 'methods',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Communicate\\V1\\Rest\\Methods\\MethodsEntity',
            'collection_class' => 'Communicate\\V1\\Rest\\Methods\\MethodsCollection',
            'service_name' => 'methods',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Communicate\\V1\\Rest\\Methods\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Communicate\\V1\\Rest\\Methods\\Controller' => array(
                0 => 'application/vnd.communicate.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Communicate\\V1\\Rest\\Methods\\Controller' => array(
                0 => 'application/vnd.communicate.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Communicate\\V1\\Rest\\Methods\\MethodsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'communicate.rest.methods',
                'route_identifier_name' => 'methods_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Communicate\\V1\\Rest\\Methods\\MethodsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'communicate.rest.methods',
                'route_identifier_name' => 'methods_id',
                'is_collection' => true,
            ),
        ),
    ),
);
