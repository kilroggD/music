<?php
namespace Communicate\V1\Rest\Methods;

class MethodsResourceFactory
{
    public function __invoke($services)
    {
        return new MethodsResource();
    }
}
