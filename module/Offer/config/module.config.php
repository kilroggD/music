<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Offer\\V1\\Rest\\Products\\ProductsResource' => 'Offer\\V1\\Rest\\Products\\ProductsResourceFactory',
            'Offer\\V1\\Rest\\Suppliers\\SuppliersResource' => 'Offer\\V1\\Rest\\Suppliers\\SuppliersResourceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'offer.rest.products' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/products[/:products_id]',
                    'defaults' => array(
                        'controller' => 'Offer\\V1\\Rest\\Products\\Controller',
                    ),
                ),
            ),
            'offer.rest.suppliers' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/suppliers[/:suppliers_id]',
                    'defaults' => array(
                        'controller' => 'Offer\\V1\\Rest\\Suppliers\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'offer.rest.products',
            1 => 'offer.rest.suppliers',
        ),
    ),
    'zf-rest' => array(
        'Offer\\V1\\Rest\\Products\\Controller' => array(
            'listener' => 'Offer\\V1\\Rest\\Products\\ProductsResource',
            'route_name' => 'offer.rest.products',
            'route_identifier_name' => 'products_id',
            'collection_name' => 'products',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Offer\\V1\\Rest\\Products\\ProductsEntity',
            'collection_class' => 'Offer\\V1\\Rest\\Products\\ProductsCollection',
            'service_name' => 'products',
        ),
        'Offer\\V1\\Rest\\Suppliers\\Controller' => array(
            'listener' => 'Offer\\V1\\Rest\\Suppliers\\SuppliersResource',
            'route_name' => 'offer.rest.suppliers',
            'route_identifier_name' => 'suppliers_id',
            'collection_name' => 'suppliers',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Offer\\V1\\Rest\\Suppliers\\SuppliersEntity',
            'collection_class' => 'Offer\\V1\\Rest\\Suppliers\\SuppliersCollection',
            'service_name' => 'suppliers',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Offer\\V1\\Rest\\Products\\Controller' => 'HalJson',
            'Offer\\V1\\Rest\\Suppliers\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Offer\\V1\\Rest\\Products\\Controller' => array(
                0 => 'application/vnd.offer.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Offer\\V1\\Rest\\Suppliers\\Controller' => array(
                0 => 'application/vnd.offer.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Offer\\V1\\Rest\\Products\\Controller' => array(
                0 => 'application/vnd.offer.v1+json',
                1 => 'application/json',
            ),
            'Offer\\V1\\Rest\\Suppliers\\Controller' => array(
                0 => 'application/vnd.offer.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Offer\\V1\\Rest\\Products\\ProductsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'offer.rest.products',
                'route_identifier_name' => 'products_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Offer\\V1\\Rest\\Products\\ProductsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'offer.rest.products',
                'route_identifier_name' => 'products_id',
                'is_collection' => true,
            ),
            'Offer\\V1\\Rest\\Suppliers\\SuppliersEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'offer.rest.suppliers',
                'route_identifier_name' => 'suppliers_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Offer\\V1\\Rest\\Suppliers\\SuppliersCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'offer.rest.suppliers',
                'route_identifier_name' => 'suppliers_id',
                'is_collection' => true,
            ),
        ),
    ),
);
