<?php
namespace Offer\V1\Rest\Suppliers;

class SuppliersResourceFactory
{
    public function __invoke($services)
    {
        return new SuppliersResource();
    }
}
