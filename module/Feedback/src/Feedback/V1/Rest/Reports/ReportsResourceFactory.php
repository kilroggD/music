<?php
namespace Feedback\V1\Rest\Reports;

class ReportsResourceFactory
{
    public function __invoke($services)
    {
        return new ReportsResource();
    }
}
