<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Feedback\\V1\\Rest\\Reports\\ReportsResource' => 'Feedback\\V1\\Rest\\Reports\\ReportsResourceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'feedback.rest.reports' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/reports[/:reports_id]',
                    'defaults' => array(
                        'controller' => 'Feedback\\V1\\Rest\\Reports\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'feedback.rest.reports',
        ),
    ),
    'zf-rest' => array(
        'Feedback\\V1\\Rest\\Reports\\Controller' => array(
            'listener' => 'Feedback\\V1\\Rest\\Reports\\ReportsResource',
            'route_name' => 'feedback.rest.reports',
            'route_identifier_name' => 'reports_id',
            'collection_name' => 'reports',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Feedback\\V1\\Rest\\Reports\\ReportsEntity',
            'collection_class' => 'Feedback\\V1\\Rest\\Reports\\ReportsCollection',
            'service_name' => 'reports',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Feedback\\V1\\Rest\\Reports\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Feedback\\V1\\Rest\\Reports\\Controller' => array(
                0 => 'application/vnd.feedback.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Feedback\\V1\\Rest\\Reports\\Controller' => array(
                0 => 'application/vnd.feedback.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Feedback\\V1\\Rest\\Reports\\ReportsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'feedback.rest.reports',
                'route_identifier_name' => 'reports_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Feedback\\V1\\Rest\\Reports\\ReportsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'feedback.rest.reports',
                'route_identifier_name' => 'reports_id',
                'is_collection' => true,
            ),
        ),
    ),
);
