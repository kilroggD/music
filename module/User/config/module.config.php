<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'User_driver' => array(
                'class' => 'Doctrine\\ORM\\Mapping\\Driver\\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    0 => 'C:\\OpenServer\\domains\\homelife\\module\\User\\config/../src/User/Entity',
                ),
            ),
            'orm_default' => array(
                'drivers' => array(
                    'User\\Entity' => 'User_driver',
                ),
            ),
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'User\\V1\\Rpc\\Registration\\Controller' => 'User\\V1\\Rpc\\Registration\\RegistrationControllerFactory',
            'User\\V1\\Rpc\\Activation\\Controller' => 'User\\V1\\Rpc\\Activation\\ActivationControllerFactory',
            'User\\V1\\Rpc\\Group\\Controller' => 'User\\V1\\Rpc\\Group\\GroupControllerFactory',
            'User\\V1\\Rpc\\Logout\\Controller' => 'User\\V1\\Rpc\\Logout\\LogoutControllerFactory',
            'User\\V1\\Rpc\\Profile\\Controller' => 'User\\V1\\Rpc\\Profile\\ProfileControllerFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'user.rpc.registration' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/register',
                    'defaults' => array(
                        'controller' => 'User\\V1\\Rpc\\Registration\\Controller',
                        'action' => 'registration',
                    ),
                ),
            ),
            'user.rpc.activation' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/activate',
                    'defaults' => array(
                        'controller' => 'User\\V1\\Rpc\\Activation\\Controller',
                        'action' => 'activation',
                    ),
                ),
            ),
            'user.rest.users' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/users[/:users_id]',
                    'defaults' => array(
                        'controller' => 'User\\V1\\Rest\\Users\\Controller',
                    ),
                ),
            ),
            'user.rpc.group' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/users/group',
                    'defaults' => array(
                        'controller' => 'User\\V1\\Rpc\\Group\\Controller',
                        'action' => 'group',
                    ),
                ),
            ),
            'user.rpc.logout' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/logout',
                    'defaults' => array(
                        'controller' => 'User\\V1\\Rpc\\Logout\\Controller',
                        'action' => 'logout',
                    ),
                ),
            ),
            'user.rpc.profile' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/me',
                    'defaults' => array(
                        'controller' => 'User\\V1\\Rpc\\Profile\\Controller',
                        'action' => 'profile',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'user.rpc.registration',
            1 => 'user.rpc.activation',
            2 => 'user.rest.users',
            3 => 'user.rpc.group',
            4 => 'user.rpc.logout',
            5 => 'user.rpc.profile',
        ),
    ),
    'zf-rpc' => array(
        'User\\V1\\Rpc\\Registration\\Controller' => array(
            'service_name' => 'Registration',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'user.rpc.registration',
        ),
        'User\\V1\\Rpc\\Activation\\Controller' => array(
            'service_name' => 'Activation',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'user.rpc.activation',
        ),
        'User\\V1\\Rpc\\Group\\Controller' => array(
            'service_name' => 'Group',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'user.rpc.group',
        ),
        'User\\V1\\Rpc\\Logout\\Controller' => array(
            'service_name' => 'Logout',
            'http_methods' => array(
                0 => 'POST',
            ),
            'route_name' => 'user.rpc.logout',
        ),
        'User\\V1\\Rpc\\Profile\\Controller' => array(
            'service_name' => 'Profile',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'user.rpc.profile',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'User\\V1\\Rpc\\Registration\\Controller' => 'Json',
            'User\\V1\\Rpc\\Activation\\Controller' => 'Json',
            'User\\V1\\Rest\\Users\\Controller' => 'HalJson',
            'User\\V1\\Rpc\\Group\\Controller' => 'Json',
            'User\\V1\\Rpc\\Logout\\Controller' => 'Json',
            'User\\V1\\Rpc\\Profile\\Controller' => 'Json',
        ),
        'accept_whitelist' => array(
            'User\\V1\\Rpc\\Registration\\Controller' => array(
                0 => 'application/vnd.user.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'User\\V1\\Rpc\\Activation\\Controller' => array(
                0 => 'application/vnd.user.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'User\\V1\\Rest\\Users\\Controller' => array(
                0 => 'application/vnd.user.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'User\\V1\\Rpc\\Group\\Controller' => array(
                0 => 'application/vnd.user.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'User\\V1\\Rpc\\Logout\\Controller' => array(
                0 => 'application/vnd.user.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'User\\V1\\Rpc\\Profile\\Controller' => array(
                0 => 'application/vnd.user.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
        ),
        'content_type_whitelist' => array(
            'User\\V1\\Rpc\\Registration\\Controller' => array(
                0 => 'application/vnd.user.v1+json',
                1 => 'application/json',
            ),
            'User\\V1\\Rpc\\Activation\\Controller' => array(
                0 => 'application/vnd.user.v1+json',
                1 => 'application/json',
            ),
            'User\\V1\\Rest\\Users\\Controller' => array(
                0 => 'application/vnd.user.v1+json',
                1 => 'application/json',
            ),
            'User\\V1\\Rpc\\Group\\Controller' => array(
                0 => 'application/vnd.user.v1+json',
                1 => 'application/json',
            ),
            'User\\V1\\Rpc\\Logout\\Controller' => array(
                0 => 'application/vnd.user.v1+json',
                1 => 'application/json',
            ),
            'User\\V1\\Rpc\\Profile\\Controller' => array(
                0 => 'application/vnd.user.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'User\\V1\\Rest\\Users\\UsersResource' => 'User\\V1\\Rest\\Users\\UsersResourceFactory',
        ),
    ),
    'zf-rest' => array(
        'User\\V1\\Rest\\Users\\Controller' => array(
            'listener' => 'User\\V1\\Rest\\Users\\UsersResource',
            'route_name' => 'user.rest.users',
            'route_identifier_name' => 'users_id',
            'collection_name' => 'users',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'User\\V1\\Rest\\Users\\UsersEntity',
            'collection_class' => 'User\\V1\\Rest\\Users\\UsersCollection',
            'service_name' => 'users',
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'User\\V1\\Rest\\Users\\UsersEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'user.rest.users',
                'route_identifier_name' => 'users_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'User\\V1\\Rest\\Users\\UsersCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'user.rest.users',
                'route_identifier_name' => 'users_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authorization' => array(
            'User\\V1\\Rpc\\Profile\\Controller' => array(
                'actions' => array(
                    'Profile' => array(
                        'GET' => true,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ),
                ),
            ),
        ),
    ),
);
