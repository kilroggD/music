<?php
namespace User\V1\Rpc\Registration;

class RegistrationControllerFactory
{
    public function __invoke($controllers)
    {
        return new RegistrationController();
    }
}
