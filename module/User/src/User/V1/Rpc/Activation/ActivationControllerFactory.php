<?php
namespace User\V1\Rpc\Activation;

class ActivationControllerFactory
{
    public function __invoke($controllers)
    {
        return new ActivationController();
    }
}
