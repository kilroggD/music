<?php

namespace User\V1\Rpc\Logout;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class LogoutController extends AbstractActionController {

    /**
     * Логаут. уничтожаем токен для перестраховки
     * @return JsonModel
     */
    public function logoutAction() {
        $accessToken = null;
        $token = null;
        $token = $this->params()->fromPost("token");
        $identity = $this->getEvent()->getParam('ZF\\MvcAuth\\Identity')->getAuthenticationIdentity();
        if ($identity["access_token"]) {
            $accessToken = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->getRepository("ZF\OAuth2\Doctrine\Entity\AccessToken")->findOneByAccessToken($identity["access_token"]);
        }
        if ($accessToken) {
          $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->remove($accessToken);
          $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')->flush();
        }
        $result = new JsonModel(array(
            'success' => true,
        ));
        return $result;
    }

}
