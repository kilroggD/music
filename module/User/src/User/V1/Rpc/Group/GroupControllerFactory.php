<?php
namespace User\V1\Rpc\Group;

class GroupControllerFactory
{
    public function __invoke($controllers)
    {
        return new GroupController();
    }
}
