<?php

return array(
    'controllers' => array(
        'factories' => array(
            'Acl\\V1\\Rpc\\ApiAccess\\Controller' => 'Acl\\V1\\Rpc\\ApiAccess\\ApiAccessControllerFactory',
            'Acl\\V1\\Rpc\\GuiAccess\\Controller' => 'Acl\\V1\\Rpc\\GuiAccess\\GuiAccessControllerFactory',
            'Acl\\V1\\Rpc\\AclUpdate\\Controller' => 'Acl\\V1\\Rpc\\AclUpdate\\AclUpdateControllerFactory',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Acl\\V1\\Rest\\Roles\\RolesResource' => 'Acl\\V1\\Rest\\Roles\\RolesResourceFactory',
            'Acl\\V1\\Rest\\Permissions\\PermissionsResource' => 'Acl\\V1\\Rest\\Permissions\\PermissionsResourceFactory',
            "aclService" => function($sm) {
                $service = new Acl\Service\AclService($sm);
                return $service;
            }
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'Acl_driver' => array(
                'class' => 'Doctrine\\ORM\\Mapping\\Driver\\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    0 => __DIR__ . '/../src/Acl/Entity',
                ),
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Acl\\Entity' => 'Acl_driver',
                ),
            ),
        ),
    ),
    'router' => array(
        'routes' => array(
            'acl.rest.roles' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/acl/roles[/:roles_id]',
                    'defaults' => array(
                        'controller' => 'Acl\\V1\\Rest\\Roles\\Controller',
                    ),
                ),
            ),
            'acl.rest.permissions' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/acl/permissions[/:permissions_id]',
                    'defaults' => array(
                        'controller' => 'Acl\\V1\\Rest\\Permissions\\Controller',
                    ),
                ),
            ),
            'acl.rpc.api-access' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/acl/api_access',
                    'defaults' => array(
                        'controller' => 'Acl\\V1\\Rpc\\ApiAccess\\Controller',
                        'action' => 'apiAccess',
                    ),
                ),
            ),
            'acl.rpc.gui-access' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/acl/gui_access',
                    'defaults' => array(
                        'controller' => 'Acl\\V1\\Rpc\\GuiAccess\\Controller',
                        'action' => 'guiAccess',
                    ),
                ),
            ),
            'acl.rpc.acl-update' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin/acl/update',
                    'defaults' => array(
                        'controller' => 'Acl\\V1\\Rpc\\AclUpdate\\Controller',
                        'action' => 'aclUpdate',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'acl.rest.roles',
            1 => 'acl.rest.permissions',
            2 => 'acl.rpc.api-access',
            3 => 'acl.rpc.gui-access',
            4 => 'acl.rpc.acl-update',
        ),
    ),
    'zf-rpc' => array(
        'Acl\\V1\\Rpc\\ApiAccess\\Controller' => array(
            'service_name' => 'ApiAccess',
            'http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'route_name' => 'acl.rpc.api-access',
        ),
        'Acl\\V1\\Rpc\\GuiAccess\\Controller' => array(
            'service_name' => 'GuiAccess',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'acl.rpc.gui-access',
        ),
        'Acl\\V1\\Rpc\\AclUpdate\\Controller' => array(
            'service_name' => 'AclUpdate',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'acl.rpc.acl-update',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Acl\\V1\\Rest\\Roles\\Controller' => 'HalJson',
            'Acl\\V1\\Rest\\Permissions\\Controller' => 'HalJson',
            'Acl\\V1\\Rpc\\ApiAccess\\Controller' => 'Json',
            'Acl\\V1\\Rpc\\GuiAccess\\Controller' => 'Json',
            'Acl\\V1\\Rpc\\AclUpdate\\Controller' => 'Json',
        ),
        'accept_whitelist' => array(
            'Acl\\V1\\Rest\\Roles\\Controller' => array(
                0 => 'application/vnd.acl.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Acl\\V1\\Rest\\Permissions\\Controller' => array(
                0 => 'application/vnd.acl.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Acl\\V1\\Rpc\\ApiAccess\\Controller' => array(
                0 => 'application/vnd.acl.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'Acl\\V1\\Rpc\\GuiAccess\\Controller' => array(
                0 => 'application/vnd.acl.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
            'Acl\\V1\\Rpc\\AclUpdate\\Controller' => array(
                0 => 'application/vnd.acl.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
        ),
        'content_type_whitelist' => array(
            'Acl\\V1\\Rest\\Roles\\Controller' => array(
                0 => 'application/vnd.acl.v1+json',
                1 => 'application/json',
            ),
            'Acl\\V1\\Rest\\Permissions\\Controller' => array(
                0 => 'application/vnd.acl.v1+json',
                1 => 'application/json',
            ),
            'Acl\\V1\\Rpc\\ApiAccess\\Controller' => array(
                0 => 'application/vnd.acl.v1+json',
                1 => 'application/json',
            ),
            'Acl\\V1\\Rpc\\GuiAccess\\Controller' => array(
                0 => 'application/vnd.acl.v1+json',
                1 => 'application/json',
            ),
            'Acl\\V1\\Rpc\\AclUpdate\\Controller' => array(
                0 => 'application/vnd.acl.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-rest' => array(
        'Acl\\V1\\Rest\\Roles\\Controller' => array(
            'listener' => 'Acl\\V1\\Rest\\Roles\\RolesResource',
            'route_name' => 'acl.rest.roles',
            'route_identifier_name' => 'roles_id',
            'collection_name' => 'roles',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Acl\\V1\\Rest\\Roles\\RolesEntity',
            'collection_class' => 'Acl\\V1\\Rest\\Roles\\RolesCollection',
            'service_name' => 'roles',
        ),
        'Acl\\V1\\Rest\\Permissions\\Controller' => array(
            'listener' => 'Acl\\V1\\Rest\\Permissions\\PermissionsResource',
            'route_name' => 'acl.rest.permissions',
            'route_identifier_name' => 'permissions_id',
            'collection_name' => 'permissions',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Acl\\V1\\Rest\\Permissions\\PermissionsEntity',
            'collection_class' => 'Acl\\V1\\Rest\\Permissions\\PermissionsCollection',
            'service_name' => 'permissions',
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Acl\\V1\\Rest\\Roles\\RolesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'acl.rest.roles',
                'route_identifier_name' => 'roles_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Acl\\V1\\Rest\\Roles\\RolesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'acl.rest.roles',
                'route_identifier_name' => 'roles_id',
                'is_collection' => true,
            ),
            'Acl\\V1\\Rest\\Permissions\\PermissionsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'acl.rest.permissions',
                'route_identifier_name' => 'permissions_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Acl\\V1\\Rest\\Permissions\\PermissionsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'acl.rest.permissions',
                'route_identifier_name' => 'permissions_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authorization' => array(
            'Acl\\V1\\Rest\\Permissions\\Controller' => array(
                'collection' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => true,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
            'Acl\\V1\\Rpc\\ApiAccess\\Controller' => array(
                'actions' => array(
                    'ApiAccess' => array(
                        'GET' => true,
                        'POST' => false,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ),
                ),
            ),
            'Acl\\V1\\Rest\\Roles\\Controller' => array(
                'collection' => array(
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
                'entity' => array(
                    'GET' => false,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ),
            ),
        ),
    ),
);
