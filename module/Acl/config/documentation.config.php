<?php
return array(
    'Acl\\V1\\Rest\\Roles\\Controller' => array(
        'description' => 'Управление доступными ролями пользователей',
        'collection' => array(
            'description' => 'Коллекция ролей',
            'GET' => array(
                'description' => 'Просмотр списка ролей',
            ),
        ),
        'entity' => array(
            'description' => 'Сущность "роль"',
            'GET' => array(
                'description' => 'Просмотр роли',
            ),
        ),
    ),
    'Acl\\V1\\Rpc\\ApiAccess\\Controller' => array(
        'description' => 'Управление доступом к API ресурсам',
        'GET' => array(
            'description' => 'получить список разрешений',
        ),
        'POST' => array(
            'description' => 'Обновить список разрешений',
        ),
    ),
);
