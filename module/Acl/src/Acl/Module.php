<?php

namespace Acl;

use ZF\Apigility\Provider\ApigilityProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Acl\Listener\AuthorizationListener;
use ZF\MvcAuth\MvcAuthEvent;
class Module implements ApigilityProviderInterface {

    public function getConfig() {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getDocumentation() {
        return include __DIR__ . '/../../config/documentation.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'ZF\Apigility\Autoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__,
                ),
            ),
        );
    }

    public function onBootstrap($e) {
        $eventManager = $e->getTarget()->getEventManager();
        $listener = new Listener\AclListener($e->getApplication()->getServiceManager());
        $eventManager->attach($listener);
        $eventManager->attach(
            MvcAuthEvent::EVENT_AUTHORIZATION,
            new AuthorizationListener,
            1000
    );
    }

}
