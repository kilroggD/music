<?php

namespace Acl\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AclApiResources
 *
 * @ORM\Table(name="acl_api_resources", indexes={@ORM\Index(name="idx_c6b7cfbee19d9ad2", columns={"service"})})
 * @ORM\Entity
 */
class AclApiResources
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="acl_api_resources_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="resource", type="string", length=255, nullable=true)
     */
    private $resource;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=255, nullable=true)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="method", type="string", length=140, nullable=true)
     */
    private $method;

    /**
     * @var string
     *
     * @ORM\Column(name="target", type="string", length=140, nullable=true)
     */
    private $target;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=140, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="system", type="smallint", nullable=true)
     */
    private $system;

    /**
     * @var integer
     *
     * @ORM\Column(name="exclude", type="smallint", nullable=true)
     */
    private $exclude;

    /**
     * @var \Acl\Entity\AclApiServices
     *
     * @ORM\ManyToOne(targetEntity="Acl\Entity\AclApiServices")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="service", referencedColumnName="id")
     * })
     */
    private $service;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Acl\Entity\AclRoles", mappedBy="apiPermissions")
     */
    private $roles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }

}

