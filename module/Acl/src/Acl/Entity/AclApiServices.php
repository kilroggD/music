<?php

namespace Acl\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AclApiServices
 *
 * @ORM\Table(name="acl_api_services")
 * @ORM\Entity
 */
class AclApiServices {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="acl_api_services_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=140, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="controller", type="string", length=140, nullable=false)
     */
    private $controller;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=140, nullable=false)
     */
    private $description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Acl\Entity\AclApiResources", mappedBy="service")
     */
    private $resources;

}
