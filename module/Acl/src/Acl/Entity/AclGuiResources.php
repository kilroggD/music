<?php

namespace Acl\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AclGuiResources
 *
 * @ORM\Table(name="acl_gui_resources")
 * @ORM\Entity
 */
class AclGuiResources
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="acl_gui_resources_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="resource", type="string", length=255, nullable=true)
     */
    private $resource;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="system", type="smallint", nullable=true)
     */
    private $system;

    /**
     * @var integer
     *
     * @ORM\Column(name="exclude", type="smallint", nullable=true)
     */
    private $exclude;

    /**
     * @var string
     *
     * @ORM\Column(name="grp", type="string", length=140, nullable=true)
     */
    private $grp;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Acl\Entity\AclRoles", mappedBy="guiResources")
     */
    private $roles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }

}

