<?php

namespace Acl\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AclRoles
 *
 * @ORM\Table(name="acl_roles")
 * @ORM\Entity
 */
class AclRoles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="acl_roles_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=140, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="built_in", type="smallint", nullable=true)
     */
    private $builtIn;

    /**
     * @var integer
     *
     * @ORM\Column(name="hidden", type="smallint", nullable=true)
     */
    private $hidden;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Acl\Entity\AclApiResources", inversedBy="roles")
     * @ORM\JoinTable(name="acl_roles_to_api_resources",
     *   joinColumns={
     *     @ORM\JoinColumn(name="roles", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="permissions", referencedColumnName="id")
     *   }
     * )
     */
    private $apiPermissions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Acl\Entity\AclGuiResources", inversedBy="roles")
     * @ORM\JoinTable(name="acl_roles_to_gui_resources",
     *   joinColumns={
     *     @ORM\JoinColumn(name="roles", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="gui_resources", referencedColumnName="id")
     *   }
     * )
     */
    private $guiPermissions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="User\Entity\Users", mappedBy="roles")
     */
    private $users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->apiPermissions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->guiPermissions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

}

