<?php

namespace Acl\Service;

use Acl\Entity\AclPermissions;
use Zend\Permissions\Acl;
use Zend\Permissions\Acl\Role\GenericRole;
use Zend\Permissions\Acl\Resource\GenericResource;

/**
 * Description of AclService
 *
 * @author user4
 */
class AclService {

    //put your code here
    private $_sm, $_em;
    protected $doc, $permissions, $services, $auth;

    public function __construct() {
        $this->doc = $this->scanDoc();
        $this->permissions = array();
        $this->services = array();
        $this->auth = array();
        $this->_sm = $sm;
        $locator = $sm;
        $em = $locator->get('doctrine.entitymanager.orm_default');
        $this->_em = $em;
    }

    /**
     * Получить ресурсы и white пермишны Acl-ки из БД
     */
    public function getDbApiResorces() {
        $permissions = $this->_em->getRepository("Acl\Entity\AclApiResources")->findAll();
        return $permissions;
    }

    public function getDbGuiResorces() {
        $permissions = $this->_em->getRepository("Acl\Entity\AclGuiResources")->findAll();
        return $permissions;
    }

    public function getServices() {
        return $this->services;
    }

    public function getPermissions() {
        return $this->permissions;
    }

    /**
     * Извлечение списка текущих пермишнов их конфига
     * @param type $config
     */
    public function formCurrentPermissions($config) {
        $zfRest = $config["zf-rest"];
        $zfRpc = $config["zf-rpc"];
        $routes = $config["router"]["routes"];
        //авторизация для методов - сохраним в переменной класса для проверки исключения методов/разрешений
        $zfAuth = $config["zf-mvc-auth"]["authorization"];
        $this->auth = $zfAuth;
        //забиваем разрешения для реста
        foreach ($zfRest as $controller => $rest) {
            $this->addRestResource($controller, $rest);
        }
        //забиваем разрешения для рпц
        foreach ($zfRpc as $controller => $rpc) {
            if (isset($rpc["route_name"])) {
                $action = $routes[$rpc["route_name"]]["options"]["defaults"]["action"];
                $rpc["action"] = $action;
            }
            $this->addRpcResource($controller, $rpc);
        }
        return $this;
    }

    /**
     * Получаем все роли из БД
     * @return type
     */
    public function getDbRoles() {
        return $this->_em->getRepository("Acl\Entity\AclRoles")->findAll();
    }

//выдернуть 1 роль
    public function getDbRole($id) {
        return $this->_em->getRepository("Acl\Entity\AclRoles")->find($id);
    }

//проверка на админа
    public function isAdmin($user) {
        if(is_array($user)) {
            $user=$user["id"];
        }
        if (is_numeric($user)) {
            $user = $this->_em->getRepository("User\Entity\Users")->find($user);
        }
        if ($user instanceof \User\Entity\Users) {
            $roles = $user->getRoles();
            return $roles->contains($this->_em->getRepository("Acl\Entity\AclRoles")->getAdmin());
        } else {
            return false;
        }
    }

    public function allowed($acl, $roles, $res, $method = null, $type = null) {
        //проверяем, разрешено ли хотя бы 1 роли это действие
        foreach ($roles as $role) {
            if ($acl->isAllowed((string) $role, $res, $action)) {
                return true;
            }
        }
        //если не нашли совпадений - действие запрещено
        return false;
    }

    //добавляем разрешение для реста - специфический синтаксис - ресурс, коллекция, метод
    private function addRestResource($controller, $rest) {
        $data = [];
        $serviceName = $rest["service_name"];
        //документация на контроллер
        $controllerDoc = isset($this->doc[$controller]) ? $this->doc[$controller] : array();
        $controllerDescr = isset($controllerDoc["description"]) ? $controllerDoc["description"] : "";
        //добавляем сервис и описание
        $this->addService($serviceName, $controllerDescr);
        $data = ["resource" => $controller, "action" => null, "type" => "REST", "exclude" => 1];
        //методы для коллекции
        if (isset($rest["collection_http_methods"])) {
            $collectionDoc = isset($controllerDoc["collection"]) ? $controllerDoc["collection"] : array();
            $descr = isset($collectionDoc["description"]) ? $collectionDoc["description"] : "";
            foreach ($rest["collection_http_methods"] as $method) {
                if (isset($collectionDoc[$method])) {
                    $descr = $collectionDoc[$method];
                }
                $data["target"] = "collection";
                $data["method"] = $method;
                $data["description"] = $descr["description"];
                if (isset($this->auth[$controller]["collection"][$method])) {
                    //исключаем ресурс из проверки если он не указан в auth
                    $data["exclude"] = (int) !$this->auth[$controller]["collection"][$method];
                }
                $this->addResource($serviceName, $data);
            }
        }
        //методы для сущности
        if (isset($rest["entity_http_methods"])) {
            $entityDoc = isset($controllerDoc["entity"]) ? $controllerDoc["entity"] : array();
            $descr = isset($entityDoc["description"]) ? $entityDoc["description"] : "";
            foreach ($rest["entity_http_methods"] as $method) {
                if (isset($entityDoc[$method])) {
                    $descr = $entityDoc[$method];
                }
                $data["target"] = "entity";
                $data["method"] = $method;
                $data["description"] = $descr["description"];
                if (isset($this->auth[$controller]["entity"][$method])) {
                    //исключаем ресурс из проверки если он не указан в auth
                    $data["exclude"] = (int) !$this->auth[$controller]["entity"][$method];
                }
                $this->addResource($serviceName, $data);
            }
        }
    }

    //Добавление ресурса для рпц - другая структура, нет разделения на коллекции и сущности
    private function addRpcResource($controller, $rpc) {
        $data = [];
        $serviceName = $rpc["service_name"];
        $controllerDoc = isset($this->doc[$controller]) ? $this->doc[$controller] : array();
        $controllerDescr = isset($controllerDoc["description"]) ? $controllerDoc["description"] : "";
        //добавляем сервис и описание
        $this->addService($serviceName, $controllerDescr);
        $data = ["resource" => $controller, "action" => $rpc["action"], "type" => "REST", "target" => null, "exclude" => 1];
        if (isset($rest["http_methods"])) {
            $descr = $controllerDescr;
            foreach ($rpc["http_methods"] as $method) {
                if (isset($controllerDoc[$method])) {
                    $descr = $controllerDoc[$method];
                }
                $data["method"] = $method;
                $data["description"] = $descr["description"];
                if (isset($this->auth[$controller]["actions"][ucfirst($data["action"])][$method])) {
                    //исключаем ресурс из проверки если он не указан в auth
                    $data["exclude"] = (int) !$this->auth[$controller]["entity"][$method];
                }
                $this->addResource($serviceName, $data);
            }
        }
    }

    //Добавление ресурса
    private function addResource($service, $data) {
        $this->permissions[$service][] = $data;
        return $this;
    }

    private function addService($name, $controller, $description) {
        $this->services[$name] = array("name" => $name, "controller" => $controller, "description" => $description);
        return $this;
    }

    //собираем документацию
    private function scanDoc() {
        $doc = array();
        foreach (glob("module/*", GLOB_ONLYDIR) as $module) {
            if (file_exists('module/' . basename($module) . '/config/documentation.config.php')) {
                $module_doc = include 'module/' . basename($module) . '/config/documentation.config.php';
                $doc = array_merge($doc, $module_doc);
            }
        }
        return $doc;
    }

}
