<?php
namespace Acl\V1\Rest\Permissions;

class PermissionsResourceFactory
{
    public function __invoke($services)
    {
        return new PermissionsResource();
    }
}
