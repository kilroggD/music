<?php
namespace Acl\V1\Rpc\GuiAccess;

class GuiAccessControllerFactory
{
    public function __invoke($controllers)
    {
        return new GuiAccessController();
    }
}
