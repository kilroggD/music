<?php

namespace Acl\V1\Rpc\AclUpdate;

use Zend\Mvc\Controller\AbstractActionController;

class AclUpdateController extends AbstractActionController {

    public function aclUpdateAction() {
        $this->getEventManager()->trigger('aclUpdate', $this);
        $load = $this->params()->fromQuery('load');
        $toDelete = array();
        $toUpdate = array();
        $delRecords = array();
        //текущие пермишны из конфига
        $cPermissions = $this->currentPermissions;
    }

}
