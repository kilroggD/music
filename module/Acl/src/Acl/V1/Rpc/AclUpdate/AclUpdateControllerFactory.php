<?php
namespace Acl\V1\Rpc\AclUpdate;

class AclUpdateControllerFactory
{
    public function __invoke($controllers)
    {
        return new AclUpdateController();
    }
}
