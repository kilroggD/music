<?php
namespace Acl\V1\Rpc\ApiAccess;

class ApiAccessControllerFactory
{
    public function __invoke($controllers)
    {
        return new ApiAccessController();
    }
}
