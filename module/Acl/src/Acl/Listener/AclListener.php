<?php

namespace Acl\Listener;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Code\Scanner\DirectoryScanner;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole;
use Zend\Permissions\Acl\Resource\GenericResource;
use \Acl\Repository\AclRolesRepository;
use Zend\Console\Request as ConsoleRequest;
use ZF\MvcAuth\MvcAuthEvent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AclListener
 * Листенер для контроля ролей и прав доступа
 * @author user4
 */
class AclListener implements \Zend\EventManager\ListenerAggregateInterface {

    //put your code here
    public function __construct() {
        ;
    }

    /*
     * Прикрепить события
     */

    public function attach(EventManagerInterface $events) {
        $sharedEvents = $events->getSharedManager();
        $this->listeners[] = $sharedEvents->attach('Acl\\V1\\Rpc\\ApiAccess\\Controller', 'apiAccess', array($this, 'onAclUpdate'), 100);
    //    $this->listeners[] = $sharedEvents->attach('Install\Controller\InstallController', 'aclInstall', array($this, 'onAclUpdate'), 100);
    }

    /**
     * Открепить события
     */
    public function detach(EventManagerInterface $events) {
        
    }

    /**
     * обновление ACL по конфигам
     * @param Event $e
     * @return boolean
     */
    public function onAclUpdate($e) {
        $controller = $e->getTarget();
        $config = $controller->getServiceLocator()->get('Config');
        try {
            $aclService = $this->_sm->get("aclService");
            $currentPermissions = $aclService->formCurrentPermissions($config)->getPermissions();
            $controller->currentPermissions = $currentPermissions;
            return true;
        } catch (\Exception $e) {
            print($e->getMessage());
            return false;
        }
    }

    /**
     * Инициализация acl по таблицам в БД
     * @param type $e
     */
    public function initAcl($e) {
        
    }

    /**
     * Грузим начальные ресурсы 
     * @param type $resources
     */
    public function addResources($resources) {
        
    }

}
