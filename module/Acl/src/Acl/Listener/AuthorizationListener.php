<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Acl\Listener;

use ZF\MvcAuth\MvcAuthEvent;

/**
 * Description of AuthorizationListener
 *
 * @author user4
 */
class AuthorizationListener {

    //put your code here
    public function __invoke(MvcAuthEvent $mvcAuthEvent) {
        /** @var \ZF\MvcAuth\Authorization\AclAuthorization $authorization */
        $authorization = $mvcAuthEvent->getAuthorizationService();
            //    var_dump($authorization);
        /**
         * Regardless of how our configuration is currently through via the Apigility UI,
         * we want to ensure that the default rule for the service we want to give access
         * to a particular identity has a DENY BY DEFAULT rule.  In our case, it will be
         * for our FooBar\V1\Rest\Foo\Controller's collection method GET.
         *
         * Naturally, if you have many versions, or many methods, you would want to build
         * some kind of logic to build all the possible strings, and push these into the
         * ACL. If this gets too cumbersome, writing an assertion would be the next best
         * approach.
         */
        $controller = $mvcAuthEvent->getTarget();
        $identity = $mvcAuthEvent->getIdentity();
    //    var_dump($identity);
        //$authorization->deny(null, 'Acl\\V1\\Rpc\\ApiAccess\\Controller::apiAccess', 'GET');
     //   $authorization->deny(null, 'Acl\\V1\\Rest\\Roles\\Controller::collection', 'GET');

       //  var_dump($identity);die;
    }

}
