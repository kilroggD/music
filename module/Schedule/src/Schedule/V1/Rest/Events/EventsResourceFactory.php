<?php
namespace Schedule\V1\Rest\Events;

class EventsResourceFactory
{
    public function __invoke($services)
    {
        return new EventsResource();
    }
}
