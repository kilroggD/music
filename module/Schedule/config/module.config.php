<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Schedule\\V1\\Rest\\Events\\EventsResource' => 'Schedule\\V1\\Rest\\Events\\EventsResourceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'schedule.rest.events' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/events[/:events_id]',
                    'defaults' => array(
                        'controller' => 'Schedule\\V1\\Rest\\Events\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'schedule.rest.events',
        ),
    ),
    'zf-rest' => array(
        'Schedule\\V1\\Rest\\Events\\Controller' => array(
            'listener' => 'Schedule\\V1\\Rest\\Events\\EventsResource',
            'route_name' => 'schedule.rest.events',
            'route_identifier_name' => 'events_id',
            'collection_name' => 'events',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Schedule\\V1\\Rest\\Events\\EventsEntity',
            'collection_class' => 'Schedule\\V1\\Rest\\Events\\EventsCollection',
            'service_name' => 'events',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Schedule\\V1\\Rest\\Events\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Schedule\\V1\\Rest\\Events\\Controller' => array(
                0 => 'application/vnd.schedule.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Schedule\\V1\\Rest\\Events\\Controller' => array(
                0 => 'application/vnd.schedule.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Schedule\\V1\\Rest\\Events\\EventsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'schedule.rest.events',
                'route_identifier_name' => 'events_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Schedule\\V1\\Rest\\Events\\EventsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'schedule.rest.events',
                'route_identifier_name' => 'events_id',
                'is_collection' => true,
            ),
        ),
    ),
);
