<?php
namespace Services\V1\Rest\Services;

class ServicesResourceFactory
{
    public function __invoke($services)
    {
        return new ServicesResource();
    }
}
