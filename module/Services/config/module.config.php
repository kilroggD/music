<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Services\\V1\\Rest\\Services\\ServicesResource' => 'Services\\V1\\Rest\\Services\\ServicesResourceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'services.rest.services' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/services[/:services_id]',
                    'defaults' => array(
                        'controller' => 'Services\\V1\\Rest\\Services\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'services.rest.services',
        ),
    ),
    'zf-rest' => array(
        'Services\\V1\\Rest\\Services\\Controller' => array(
            'listener' => 'Services\\V1\\Rest\\Services\\ServicesResource',
            'route_name' => 'services.rest.services',
            'route_identifier_name' => 'services_id',
            'collection_name' => 'services',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Services\\V1\\Rest\\Services\\ServicesEntity',
            'collection_class' => 'Services\\V1\\Rest\\Services\\ServicesCollection',
            'service_name' => 'services',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Services\\V1\\Rest\\Services\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Services\\V1\\Rest\\Services\\Controller' => array(
                0 => 'application/vnd.services.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Services\\V1\\Rest\\Services\\Controller' => array(
                0 => 'application/vnd.services.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Services\\V1\\Rest\\Services\\ServicesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'services.rest.services',
                'route_identifier_name' => 'services_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Services\\V1\\Rest\\Services\\ServicesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'services.rest.services',
                'route_identifier_name' => 'services_id',
                'is_collection' => true,
            ),
        ),
    ),
);
