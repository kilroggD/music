<?php

namespace Install\Controller;

use Zend\Mvc\MvcEvent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InstallController
 *
 * @author kopychev
 */
class InstallController extends \Zend\Mvc\Controller\AbstractActionController {

    //put your code here
    private $em, $dm;
    public $config, $userParams;

    /**
     * Текущие разрешения из конфига
     * @var array 
     */
    public $currentPermissions;

    /**
     * Варианты состояния подключения к постгре
     * @var array 
     */
    private static $pgState = array("Не удалось подключиться к PostgreSQL. Проверьте настройки в config/autoload/db.local.php", "Успешное подключение");

    /**
     * Варианты состояния подключения к монге
     * @var array 
     */
    private static $mongoState = array("Не удалось подключиться к MongoDB. Проверьте настройки в config/autoload/db.local.php", "Успешное подключение");

    /**
     * Генерация монго
     */
    public function installAction() {
        $request = $this->getRequest();
        $srv = new \Install\Service\DbService($this->getEntityManager(), null);
        $msrv = new \Install\Service\MigrationService($this->getEntityManager());
        $msrv->updateEntity();
        die;
    }

    /**
     * Вернуть менеджер сущностей DoctrineORM
     * @return Doctrine\ORM\EntityManager
     */
    protected function getEntityManager() {
        if (null === $this->em) {
            $this->setEntityManager($this->getServiceLocator()->get('doctrine.entitymanager.orm_default'));
        }
        return $this->em;
    }

    /**
     * Установить менеджер сущностей DoctrineORM
     * @param type $em
     * @return \User\Controller\MyAbstractController
     */
    protected function setEntityManager($em) {
        $this->em = $em;
        return $this;
    }

    protected function getDocumentManager() {
        if (null === $this->dm) {
            $this->setDocumentManager($this->getServiceLocator()->get('doctrine.documentmanager.odm_default'));
        }
        return $this->dm;
    }

    protected function setDocumentManager($dm) {
        $this->dm = $dm;
        return $this;
    }

}
