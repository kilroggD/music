<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Install\Controller\Install' => 'Install\Controller\InstallController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'install' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/install',
                    'defaults' => array(
                        'controller' => 'Install\Controller\Install',
                        'action' => 'install'
                    ),
                ),
                'may_terminate' => true
            )
        )
    ),
        'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __DIR__ . '/../public',
            ),
        ),
    ),
);
