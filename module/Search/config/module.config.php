<?php
return array(
    'controllers' => array(
        'factories' => array(
            'Search\\V1\\Rpc\\Search\\Controller' => 'Search\\V1\\Rpc\\Search\\SearchControllerFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'search.rpc.search' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/search',
                    'defaults' => array(
                        'controller' => 'Search\\V1\\Rpc\\Search\\Controller',
                        'action' => 'search',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'search.rpc.search',
        ),
    ),
    'zf-rpc' => array(
        'Search\\V1\\Rpc\\Search\\Controller' => array(
            'service_name' => 'Search',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'search.rpc.search',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Search\\V1\\Rpc\\Search\\Controller' => 'Json',
        ),
        'accept_whitelist' => array(
            'Search\\V1\\Rpc\\Search\\Controller' => array(
                0 => 'application/vnd.search.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
        ),
        'content_type_whitelist' => array(
            'Search\\V1\\Rpc\\Search\\Controller' => array(
                0 => 'application/vnd.search.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
);
