<?php
namespace Application\Repository;
use Doctrine\ORM\EntityRepository;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Абстрактный класс для реализации поиска по репозиториям для последющей пагинации
 *
 * @author user4
 */
abstract class MyAbstractRepository  extends EntityRepository{
    //put your code here
    /**
     * Параметры для сортировки по умолчанию
     * @var array 
     */
    protected $sortParams=array("sort","dir");
    /**
     * Параметры для номера страниц по умолчанию
     * @var array 
     */
    protected $pageParams=array("page");
    
    
    
}
