<?php

return array(
    'service_manager' => array(
        'factories' => array(
            'Category\\V1\\Rest\\Categories\\CategoriesResource' => 'Category\\V1\\Rest\\Categories\\CategoriesResourceFactory',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'Category_driver' => array(
                'class' => 'Doctrine\\ORM\\Mapping\\Driver\\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    0 =>__DIR__.'/../src/Category/Entity',
                ),
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Category\\Entity' => 'Category_driver',
                ),
            ),
        ),
    ),
    'router' => array(
        'routes' => array(
            'category.rest.categories' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/categories[/:categories_id]',
                    'defaults' => array(
                        'controller' => 'Category\\V1\\Rest\\Categories\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'category.rest.categories',
        ),
    ),
    'zf-rest' => array(
        'Category\\V1\\Rest\\Categories\\Controller' => array(
            'listener' => 'Category\\V1\\Rest\\Categories\\CategoriesResource',
            'route_name' => 'category.rest.categories',
            'route_identifier_name' => 'categories_id',
            'collection_name' => 'categories',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Category\\V1\\Rest\\Categories\\CategoriesEntity',
            'collection_class' => 'Category\\V1\\Rest\\Categories\\CategoriesCollection',
            'service_name' => 'categories',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Category\\V1\\Rest\\Categories\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Category\\V1\\Rest\\Categories\\Controller' => array(
                0 => 'application/vnd.category.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Category\\V1\\Rest\\Categories\\Controller' => array(
                0 => 'application/vnd.category.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Category\\V1\\Rest\\Categories\\CategoriesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'category.rest.categories',
                'route_identifier_name' => 'categories_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Category\\V1\\Rest\\Categories\\CategoriesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'category.rest.categories',
                'route_identifier_name' => 'categories_id',
                'is_collection' => true,
            ),
        ),
    ),
);
