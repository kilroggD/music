<?php
namespace Expert\V1\Rpc\MyCategories;

class MyCategoriesControllerFactory
{
    public function __invoke($controllers)
    {
        return new MyCategoriesController();
    }
}
