<?php
return array(
    'controllers' => array(
        'factories' => array(
            'Expert\\V1\\Rpc\\MyCategories\\Controller' => 'Expert\\V1\\Rpc\\MyCategories\\MyCategoriesControllerFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'expert.rpc.my-categories' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/mycategories',
                    'defaults' => array(
                        'controller' => 'Expert\\V1\\Rpc\\MyCategories\\Controller',
                        'action' => 'myCategories',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'expert.rpc.my-categories',
        ),
    ),
    'zf-rpc' => array(
        'Expert\\V1\\Rpc\\MyCategories\\Controller' => array(
            'service_name' => 'MyCategories',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'expert.rpc.my-categories',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Expert\\V1\\Rpc\\MyCategories\\Controller' => 'Json',
        ),
        'accept_whitelist' => array(
            'Expert\\V1\\Rpc\\MyCategories\\Controller' => array(
                0 => 'application/vnd.expert.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
        ),
        'content_type_whitelist' => array(
            'Expert\\V1\\Rpc\\MyCategories\\Controller' => array(
                0 => 'application/vnd.expert.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
);
