<?php
namespace Request\V1\Rest\Requests;

class RequestsResourceFactory
{
    public function __invoke($services)
    {
        return new RequestsResource();
    }
}
