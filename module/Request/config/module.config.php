<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Request\\V1\\Rest\\Requests\\RequestsResource' => 'Request\\V1\\Rest\\Requests\\RequestsResourceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'request.rest.requests' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/requests[/:requests_id]',
                    'defaults' => array(
                        'controller' => 'Request\\V1\\Rest\\Requests\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'request.rest.requests',
        ),
    ),
    'zf-rest' => array(
        'Request\\V1\\Rest\\Requests\\Controller' => array(
            'listener' => 'Request\\V1\\Rest\\Requests\\RequestsResource',
            'route_name' => 'request.rest.requests',
            'route_identifier_name' => 'requests_id',
            'collection_name' => 'requests',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Request\\V1\\Rest\\Requests\\RequestsEntity',
            'collection_class' => 'Request\\V1\\Rest\\Requests\\RequestsCollection',
            'service_name' => 'requests',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Request\\V1\\Rest\\Requests\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Request\\V1\\Rest\\Requests\\Controller' => array(
                0 => 'application/vnd.request.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Request\\V1\\Rest\\Requests\\Controller' => array(
                0 => 'application/vnd.request.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Request\\V1\\Rest\\Requests\\RequestsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'request.rest.requests',
                'route_identifier_name' => 'requests_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Request\\V1\\Rest\\Requests\\RequestsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'request.rest.requests',
                'route_identifier_name' => 'requests_id',
                'is_collection' => true,
            ),
        ),
    ),
);
