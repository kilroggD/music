<?php
namespace Payments\V1\Rest\Invoices;

class InvoicesResourceFactory
{
    public function __invoke($services)
    {
        return new InvoicesResource();
    }
}
