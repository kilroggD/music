<?php
namespace Payments\V1\Rest\Payments;

class PaymentsResourceFactory
{
    public function __invoke($services)
    {
        return new PaymentsResource();
    }
}
