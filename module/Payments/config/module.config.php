<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Payments\\V1\\Rest\\Invoices\\InvoicesResource' => 'Payments\\V1\\Rest\\Invoices\\InvoicesResourceFactory',
            'Payments\\V1\\Rest\\Payments\\PaymentsResource' => 'Payments\\V1\\Rest\\Payments\\PaymentsResourceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'payments.rest.invoices' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/invoices[/:invoices_id]',
                    'defaults' => array(
                        'controller' => 'Payments\\V1\\Rest\\Invoices\\Controller',
                    ),
                ),
            ),
            'payments.rest.payments' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/payments[/:payments_id]',
                    'defaults' => array(
                        'controller' => 'Payments\\V1\\Rest\\Payments\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'payments.rest.invoices',
            1 => 'payments.rest.payments',
        ),
    ),
    'zf-rest' => array(
        'Payments\\V1\\Rest\\Invoices\\Controller' => array(
            'listener' => 'Payments\\V1\\Rest\\Invoices\\InvoicesResource',
            'route_name' => 'payments.rest.invoices',
            'route_identifier_name' => 'invoices_id',
            'collection_name' => 'invoices',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Payments\\V1\\Rest\\Invoices\\InvoicesEntity',
            'collection_class' => 'Payments\\V1\\Rest\\Invoices\\InvoicesCollection',
            'service_name' => 'invoices',
        ),
        'Payments\\V1\\Rest\\Payments\\Controller' => array(
            'listener' => 'Payments\\V1\\Rest\\Payments\\PaymentsResource',
            'route_name' => 'payments.rest.payments',
            'route_identifier_name' => 'payments_id',
            'collection_name' => 'payments',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Payments\\V1\\Rest\\Payments\\PaymentsEntity',
            'collection_class' => 'Payments\\V1\\Rest\\Payments\\PaymentsCollection',
            'service_name' => 'payments',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Payments\\V1\\Rest\\Invoices\\Controller' => 'HalJson',
            'Payments\\V1\\Rest\\Payments\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Payments\\V1\\Rest\\Invoices\\Controller' => array(
                0 => 'application/vnd.payments.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Payments\\V1\\Rest\\Payments\\Controller' => array(
                0 => 'application/vnd.payments.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Payments\\V1\\Rest\\Invoices\\Controller' => array(
                0 => 'application/vnd.payments.v1+json',
                1 => 'application/json',
            ),
            'Payments\\V1\\Rest\\Payments\\Controller' => array(
                0 => 'application/vnd.payments.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Payments\\V1\\Rest\\Invoices\\InvoicesEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'payments.rest.invoices',
                'route_identifier_name' => 'invoices_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Payments\\V1\\Rest\\Invoices\\InvoicesCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'payments.rest.invoices',
                'route_identifier_name' => 'invoices_id',
                'is_collection' => true,
            ),
            'Payments\\V1\\Rest\\Payments\\PaymentsEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'payments.rest.payments',
                'route_identifier_name' => 'payments_id',
                'hydrator' => 'Zend\\Hydrator\\ArraySerializable',
            ),
            'Payments\\V1\\Rest\\Payments\\PaymentsCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'payments.rest.payments',
                'route_identifier_name' => 'payments_id',
                'is_collection' => true,
            ),
        ),
    ),
);
