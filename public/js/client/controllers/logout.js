angular.module('MyApp')
        .controller('LogoutCtrl', function ($location, $auth, toastr, $http) {
            if (!$auth.isAuthenticated()) {
                return;
            }
            /* */
            // Simple GET request example:
            $http({
                method: 'POST',
                url: '/logout',
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available\
               /* console.log($auth.getToken());
                return;*/
                $auth.logout()
                        .then(function () {
                            toastr.info('You have been logged out');
                            $location.path('/');
                        });
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });

        });