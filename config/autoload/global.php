<?php

$paths = array();
foreach (glob("module/*", GLOB_ONLYDIR) as $key=>$module) {
    if (file_exists('module/' . basename($module) . '/yml')) {
        $paths[]='./module/' . basename($module) . '/yml';
    }
}
//print_r($paths);
return array(
    'router' => array(
        'routes' => array(
            'oauth' => array(
                'options' => array(
                    'spec' => '%oauth%',
                    'regex' => '(?P<oauth>(/oauth))',
                ),
                'type' => 'regex',
            ),
        ),
    ),
    'doctrine' => array(
        'configuration' => array(
            'orm_default' => array(
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'driver' => 'orm_default',
                'generate_proxies' => true,
                'proxy_dir' => 'data/DoctrineORMModule/Proxy',
                'proxy_namespace' => 'DoctrineORMModule\\Proxy',
                'filters' => array(),
            ),
            'odm_default' => array(
                'metadata_cache' => 'array',
                'driver' => 'odm_default',
                'generate_proxies' => true,
                'proxy_dir' => 'data/DoctrineMongoODMModule/Proxy',
                'proxy_namespace' => 'DoctrineMongoODMModule\\Proxy',
                'generate_hydrators' => true,
                'hydrator_dir' => 'data/DoctrineMongoODMModule/Hydrator',
                'hydrator_namespace' => 'DoctrineMongoODMModule\\Hydrator',
                'default_db' => 'scimc',
                'filters' => array(),
            ),
        ),
        'driver' => array(
            'ApplicationYamlDriver' => array(
                'class' => 'Doctrine\\ORM\\Mapping\\Driver\\YamlDriver',
                'cache' => 'array',
                'extension' => '.dcm.yml',
                'paths' =>$paths
            ),
            'MongoYamlDriver' => array(
                'class' => 'Doctrine\\ODM\\MongoDB\\Mapping\\Driver\\YamlDriver',
                'cache' => 'array',
                'extension' => '.dcm.yml',
                'paths' => array(
                    0 => './yml_m',
                ),
            ),
            'orm_default' => array(
                'class' => 'Doctrine\\ORM\\Mapping\\Driver\\DriverChain',
                'drivers' => array(
                    "Category\Entity"=>"ApplicationYamlDriver"
                ),
            ),
        ),
        'entitymanager' => array(
            'orm_default' => array(
                'connection' => 'orm_default',
                'configuration' => 'orm_default',
            ),
        ),
        'eventmanager' => array(
            'orm_default' => array(
                'subscribers' => array(
                    0 => 'Gedmo\\Tree\\TreeListener',
                    1 => 'Gedmo\\Timestampable\\TimestampableListener',
                    2 => 'Gedmo\\Sluggable\\SluggableListener',
                    3 => 'Gedmo\\Loggable\\LoggableListener',
                    4 => 'Gedmo\\Sortable\\SortableListener',
                ),
            ),
        ),
        'sql_logger_collector' => array(
            'orm_default' => array(),
        ),
        'entity_resolver' => array(
            'orm_default' => array(),
        ),
        'migrations_configuration' => array(
            'orm_default' => array(
                'directory' => './sql/migrations',
                'name' => 'Doctrine Database Migrations',
                'namespace' => 'DoctrineORMModule\\Migrations',
                'table' => 'migrations',
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authentication' => array(
            'adapters' => array(
                'oauth2_doctrine' => array(
                    'adapter' => 'ZF\\MvcAuth\\Authentication\\OAuth2Adapter',
                    'storage' => array(
                        'storage' => 'oauth2.doctrineadapter.default',
                        'route' => '/oauth',
                    ),
                ),
            ),
            'map' => array(
                'Status\\V1' => 'oauth2_doctrine',
                'Music\\V1' => 'oauth2_doctrine',
                'Acl\\V1' => 'oauth2_doctrine',
                'User\\V1' => 'oauth2_doctrine',
            ),
        ),
    ),
    'zf-oauth2' => array(
        'storage' => 'oauth2.doctrineadapter.default',
        'allow_implicit' => true,
    ),
    'db' => array(
        'adapters' => array(
            'db1' => array(),
        ),
    ),
);
